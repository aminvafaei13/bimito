/**
 * Created by Mohammad Amin on 12/19/2017.
 */
import React from 'react'
import {Row, Col} from 'react-bootstrap'
import {DatePicker} from 'antd';
import {connect} from "react-redux";
import {ChangeStep} from "../../actions/ProductsAction";
const {MonthPicker} = DatePicker;

@connect((store) => {

    return {
        step: store.product.step
    }
})

export default class years extends React.Component {
    constructor(props) {
        super(props);

        this.change = this.change.bind(this);
    }

    change() {
        this.props.dispatch(ChangeStep(0));
    }

    render() {

        return (

            <Row>
                <MonthPicker size="lg" placeholder="Select Year" className="YearPicker"/>
            </Row>)
    }
}