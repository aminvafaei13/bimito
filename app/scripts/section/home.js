/**
 * Created by Mohammad Amin on 12/18/2017.
 */
import React from 'react'
import {connect} from "react-redux";
import {Row, Col} from 'react-bootstrap';
import Products from './products';
import Cyls from './Cyls';
import Years from './years';
import {fetchProducts, ChangeStep} from "../../actions/ProductsAction";
import {Steps,message} from 'antd';

const Step = Steps.Step;
@connect((store) => {

    return {
        products: store.product.products,
        motor: store.product.motor,
        step: store.product.step
    }
})



export default class Home extends React.Component {
    constructor(props) {
        super(props);
        this.change = this.change.bind(this);
    }

    componentWillMount() {
        this.props.dispatch(fetchProducts());
        this.props.dispatch(ChangeStep(0));
    }

    change(step) {
        let t_step = this.props.step;

        if(t_step >= step){
        this.props.dispatch(ChangeStep(step));
        }else{
            message.warning('Warning!!! Please choose an Option',3);
        }
    }

    render() {
        var active = this.props.step;

        return (<Col md={12}>
            <div className="BimitoStep">
                <Steps current={this.props.step}>
                    <Step title="First Step " description="Choose your car" onClick={() => {
                        this.change(0)
                    }}/>
                    <Step title="Second Step" description="Choose Motor type" onClick={() => {
                        this.change(1)
                    }}/>
                    <Step title="Third Step" description="Car Year" onClick={() => {
                        this.change(2)
                    }}/>
                </Steps>
            </div>
            {active === 0 ? (
                <Products data={this.props.products}/>
            ) : active === 1 ? (
                <Cyls data={this.props.motor}/>
            ) : active === 2 ? (
                <Years/>
            ) : null}
        </Col>)

    }
}

