/**
 * Created by Mohammad Amin on 12/19/2017.
 */
import React from 'react'

import {Row, Col} from 'react-bootstrap'
import Product from './product';
import {connect} from "react-redux";
import {ChangeStep} from "../../actions/ProductsAction";

@connect((store) => {

    return {
        step: store.product.step
    }
})

export default class Products extends React.Component {
    constructor(props) {
        super(props);

        this.renderRow = this.renderRow.bind(this);
        this.change = this.change.bind(this);
    }

    change() {
        this.props.dispatch(ChangeStep(1));
    }

    renderRow(key) {
        return (<Col key={key} md={2} sm={4} xs={6} onClick={this.change}>
            <Product details={this.props.data[key]}/>
        </Col>)
    }

    render() {
        return (

            <Row>
                {Object.keys(this.props.data).map(this.renderRow)}
            </Row>)
    }
}