/**
 * Created by Mohammad Amin on 12/19/2017.
 */
import React from 'react'
import {Image} from 'react-bootstrap';

export default class Cyl extends React.Component {
    constructor(props) {
        super(props);



    }


    render() {
        let details = this.props.details;
        return (

            <div className="Cars"  >
                <Image src={"styles/asstes/cyl.png"} circle className="CarImage"/>
                <p className="CarName">{details.Mtype}</p>
            </div>

        )
    }
}