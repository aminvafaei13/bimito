/**
 * Created by Mohammad Amin on 12/19/2017.
 */
import React from 'react'
import {Image} from 'react-bootstrap';

export default class Product extends React.Component {
    constructor(props) {
        super(props);


        this.handleClick = this.handleClick.bind(this);
    }

    handleClick(e) {
        e.preventDefault();

    }

    render() {
        let details = this.props.details;
        return (

            <div className="Cars" onClick={this.handleClick}>
                <Image src={details.icon} circle className="CarImage"/>
                <p className="CarName">{details.latinName}</p>
            </div>

        )
    }
}