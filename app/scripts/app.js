/**
 * Created by Mohammad Amin on 12/18/2017.
 */
import React from 'react'

import {Row, Col} from 'react-bootstrap'
import {Container} from 'reactstrap';
import {Provider} from "react-redux";
import store from "./store";
import Main from './main';



export default class App extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <div>
                    {/*<Col lg={12}>*/}
                        {/*<Header/>*/}
                        <Container>
                            {/*<Row>*/}
                                <Main/>
                            {/*</Row>*/}
                        </Container>
                        {/*<Footer/>*/}
                    {/*</Col>*/}
                </div>
            </Provider>
        )
    }
}