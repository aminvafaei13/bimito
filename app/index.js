/**
 * Created by Mohammad Amin on 12/17/2017.
 */
import React from 'react'
import ReactDom from 'react-dom'
import {BrowserRouter} from 'react-router-dom'
import 'bootstrap/dist/css/bootstrap.css'

import App from  './scripts/app'
const routes = (
    <BrowserRouter>
        <App/>
    </BrowserRouter>
)
ReactDom.render(routes, document.getElementById('bimitoRoot'))


