/**
 * Created by Mohammad Amin on 12/21/2017.
 */
export default function reducer(state={
    products:[],
    step:0,
    motor:[],
    fetching: false,
    fetched: false,
    error: null
}, action){
    switch (action.type) {
        case "FETCH_USERS":{
            return {...state, fetching: true}
        }
        case "FETCH_PRODUCTS_FULFILLED":{
            return {
                ...state,
                fetching: false,
                fetched: true,
                products:action.payload,
                motor:action.motor
            }
        }
        case "FETCH_PRODUCTS_REJECTED":{
            return {
                ...state,
                fetching: false,
                products: action.payload
            }
        }
        case "CHANGESTEP":{
        return {
            ...state,
            step: action.step,

        }
    }

    }
    return state;
}