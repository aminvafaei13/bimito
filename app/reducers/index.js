/**
 * Created by Mohammad Amin on 12/21/2017.
 */
import {combineReducers} from "redux";

import product from "./ProductReducers";

export default combineReducers({
    product
})