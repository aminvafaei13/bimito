/**
 * Created by Mohammad Amin on 12/21/2017.
 */
import axios from "axios";

export function fetchProducts(){

    return function(dispatch){
       let motor = [{Mtype:'4 cylinder '},{Mtype:'6 cylinder '}];
        axios.post('https://www.bimito.com/bimito/core/hc/getCarTypes')
            .then((response) =>{
                dispatch({type:'FETCH_PRODUCTS_FULFILLED', payload:response.data.result,motor:motor});
            })
            .catch((err) => {
                dispatch({type:'FETCH_PRODUCTS_REJECTED',payload:err})
            })
    }
}

export function ChangeStep(step){

    return function(dispatch){
        dispatch({type:'CHANGESTEP', step:step});
    }
}