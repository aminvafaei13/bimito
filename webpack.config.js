const webpack = require('webpack')
const path = require('path')
module.exports = {
    entry: './app/index.js',
    output: {
        filename: "./build/bundle.js",
        sourceMapFilename: "./build/bundle.map"
    },
    devtool: '#source-map',
    module: {
        loaders: [
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/
            }, {
                test: /\.css$/,
                loader: 'style-loader!css-loader'
            },
            {
                test: /\.(png|jpg|gif|svg|eot|ttf|woff|woff2)$/,
                loader: 'url-loader',
                options: {
                    limit: 10000
                }
            }
        ]
    }
}
